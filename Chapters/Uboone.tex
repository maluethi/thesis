% Chapter 2
\chapter{MicroBooNE} % Chapter title
\begin{figure}[h]
	\centering
	\includegraphics{gfx/uboone/aerial_diagram.pdf} 
	\caption[Aerial view of the MicroBooNE experiment]{Aerial view of the booster neutrino beamline with MicroBooNE situated in the middle of the image, \SI{473}{\meter} downstream from the neutrino source (Booster Targer). The neutrino beam is shown as a dashed red line.}
	\label{fig:overview}
\end{figure}
The MicroBooNE detector at Fermilab in Batavia Illinois, is hosted in the Liquid Argon Test Facility (LArTF), \SI{470}{\meter} downstream of the neutrino production target.
Neutrinos with a peak energy of \SI{700}{\mega\electronvolt} are produced in the Booster Neutrino Beam (BNB) mainly by decaying pions, produced by protons impinged onto a beryllium target. 
The neutrinos are then detected in a large scale \ac{LArTPC}.
It's main physical goal is to understand the nature of the \ac{LEE} observed in MiniBooNE (see \autoref{nu:sub:landscape}), in contrast to MiniBooNE, \acp{LArTPC} are capable of distinguishing between electrons and photons produced in electron neutrino interactions.
For the objective of a precision measurement of the neutrino signal it is crucial to characterize the electric field in the detector. 
This will be achieved by a laser calibration system developed in the scope of this thesis.
Additionally, MicroBooNE will perform neutrino cross-section measurements helping to obtain a clearer picture of neutrino interactions with argon.
Each sub-system of the experiment is described in the sections below, tightly following the main publication of the detector construction \cite{DetectorPaper}.

\section{Booster Neutrino Beam}
\label{sec:BNB}
The \ac{BNB} is an evolution of the neutrino horn proposed and realized by Van der Meer \cite{vanderMeer} at CERN.
An in detail description of the \ac{BNB} can be found in \cite{Stancu:2001cpa, PhysRevD.79.072002}, the following discussion is based on these references.
Protons with an energy of \SI{8}{\giga\electronvolt} are extracted from the Fermilab booster rapid-cycling synchrotron at a rate of \SI{5}{\hertz} 
The extracted protons are impinged onto a beryllium target, in the target interactions mainly pions and a small fraction of kaons are produced.
The target has the shape of a long rod with a length of \SI{71.1}{\centi\meter} and a radius of \SI{0.51}{\centi\meter}.
Its form is governed by the absorption length of protons in the target, so to optimize the conversion of protons into mesons.
Although, the incident high energetic proton governs the forward direction of the decay products, neutrino intensities directed at the MicroBooNE detector would still be too low to conduct neutrino studies within an acceptable time-frame.
Therefore, the beryllium target is placed inside a magnet focusing the decay products into a beam towards the detector.
The focusing electro-magnet is made of aluminum and has a toroidal shape, it is pulsed with a peak current of \SI{174}{\kilo\ampere} synchronized with the arrival of a proton beam bunch.
The magnetic horn consists of two axially-symmetric conductors with a current sheet running down the inner conductor and returning on the outer conductor. 
Between the conductors a torodial magnetic field is produced, the resulting force $F = q\vec{v} \times \vec{B}$ from the magnetic field acts as a restoring force, thus focusing particles of one sign ($\pi^+$ or $\pi^-$) and defocusing particles of the other sign.
Thus, the horn construction not only enhances the pion flux towards the detector but also selects the sign.
Pions produced, selected for sign and focused in the horn will eventually decay into a muon and a neutrino according to \autoref{eq:pidec}.
For this a \SI{50}{\meter} long decay tunnel filled with air is foreseen.
The emitted muon is then absorbed at the end of the decay tunnel before decaying, preventing the electron neutrinos emitted in the muon decay to pollute the pure muon neutrino beam. 
Although optimized for muon neutrino purity, other neutrino flavours can leak into the beam content, due to the decay of the muon inside the decay tunnel or very forward going pions (which are not defocused by the horn) of the wrong sign.
Furthermore, primary kaons produced in addition to the pions have several decay channels leading to the production of electron neutrinos.
The discussed principle of the neutrino beam generation is illustrated in \autoref{fig:beam_principle} and the expected neutrino energy at the MicroBooNE detector is shown in \autoref{fig:mu-flux}.
\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{gfx/uboone/beam-principle.pdf} 
	\caption[Booster neutrino beam operaion principle]{Beam operation principle, an incident proton beam produces mainly pions in a beryllium target. The pions of the positive charge are focused in the horn, whereas the negative pions are defocused. Finally the pion decays into a muon and a muon neutrino. The muon is absorbed at the end of the decay tunnel and the neutrino travels to the detector, where it hopefully interacts.}
	\label{fig:beam_principle}
\end{figure}
\begin{figure}[htbp]
	\centering
	\includegraphics[width=\textwidth]{gfx/uboone/mu-flux.pdf} 
	\caption[Neutrino flux in MicroBooNE]{Flux for different neutrino flavors in MicroBooNE. Adapted from \cite{SBND}.}
	\label{fig:mu-flux}
\end{figure}





\section{Cryogenic System}
The temperature range of \SIrange{83}{87}{\kelvin}, where argon is in the liquid aggregate state at normal pressure, requires cryogenic systems to operate a \ac{LArTPC}. 
The system is complicated by the requirement of low levels of oxygen equivalent impurities has to be achieved and maintained.

The cryostat itself has three major components: a stainless steel vessel hosting all the liquid argon and the detector, front and rear support structure and \SI{41}{\centi\meter} foam insulation, restricting heat input from ambient air, covering the entire cryostat surface.
The cryostat is of cylindrical shape enclosed by domed caps closing each end, with a total length of \SI{12.2}{\meter} and a diameter of \SI{3.81}{\meter} and a wall thickness of \SI{11.1}{\milli\meter}.
Requiring a low level of oxygen equivalent impurities of \SI{100}{ppt}, corresponding to a electron lifetime of roughly \SI{3}{\milli\second}, requires the minimization of outgassing within the gas phase and avoid leakage and diffusion of air into the cryostat.
Furthermore, to maintain the high liquid argon purity it is necessary to constantly remove these impurities.
For this purpose a re-circulation and filter system is build, consisting of two identical subsystems.
Each system deploying a liquid argon pump\footnote{Barber-Nichols BNCP-32B-000 magnetically-driven partial-emission centrifugal pump} and two distinct filters, using different adsorber materials\footnote{Molecular sieve supplied by Sigma-Aldrich}$^{,}$\footnote{BASF CU-0226 S, a pelletized material of copper impregnated on a high-surface-area alumina}, removing mainly water or oxygen, respectively. 
Impurity concentration is monitored with two purity monitors \cite{CARUGNO1990580} immersed in the liquid argon within the cryostat volume.
The filters are regenerated in-situ and as needed, removing the acquired and bound oxygen and water by heating the filter material while flushing with an argon-hydrogen gas mixture.
This exothermic reaction forms water with the captured oxygen that is sub-sequentially flushed out with argon gas.
The heat input to the cryostat by the ambient air as well as the detector electronics immersed in the not instrumented liquid argon leads to evaporation. 
To maintain the liquid argon level constant and to keep temperature and pressure inside the cryostat constant, a nitrogen refrigeration system is deployed.
This system contains two condensers in parallel, one for operational use and another one for backup.
Each condenser contains two liquid nitrogen coils, an inner and an outer, with the gas argon on the shell side.
The system can handle a heat load of up to \SI{6}{\kilo\watt}, using \SI{3400}{\liter} of liquid nitrogen per day.

\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{gfx/uboone/cryogenics.jpg} 
	\caption[MicroBooNE cryogenic system overview.]{MicroBooNE cryogenic system overview. Centrally (in white) the cryostat is shown sitting on the front and rear support structure. Below the cryostat, the filter and recirculation components are placed. The (re-)filling system and refrigeration components are hosted on the ground platform on the top right of the figure. Right above the cryostat a platform is installed, hosting computer racks for readout and control systems. \cite{DetectorPaper}.}
	\label{fig:cryogenics}
\end{figure}


\section{Time-Projection Chamber}
\label{ub:tpc}
The \ac{LArTPC} is composed of three main structures: the cathode, the field cage, and the anode. 
A negative voltage is introduced via a feedthrough passing through the cryostat connected to the cathode, defining the electric potential at the cathode.
A uniform electric field between the cathode and the anode planes is achieved by a series of field cage rings connected by a voltage divider chain starting at the cathode and ending at the anode
plane.
The field cage structure is held in place by a G-10\footnote{A glass epoxy composite laminate material well suited for liquid argon temperatures} support structure.
Facing the cathode planes are the sense wire planes: two induction planes (referred to as the “U” and “V” planes) with wires oriented at \SI{\pm 60}{\deg} from vertical, followed by one collection plane (referred to as the “Y” plane) with vertically-oriented wires. 
The wires of the anode planes are the sensing elements that detect the ionization created by charged particles traveling through the \ac{LArTPC}.
\autoref{fig:uboonetpc} depicts the assembled MicroBooNE \ac{LArTPC} after insertion into the cryostat, showing details of the cathode, field cage, and anode plane. 
\autoref{tab:tpcparam} lists the main parameters of the MicroBooNE \ac{LArTPC}.

\begin{table}[!htb]
	\centering
	\begin{tabular}{lr} % Column formatting, @{} suppresses leading/trailing space
		\hline
		\spacedlowsmallcaps{Parameter} & \spacedlowsmallcaps{Value} \\
		\hline
		\ac{LArTPC} (active) dimensions ($h\times w\times l$) & 2.325 m $\times$ 2.560 m $\times$ 10.368 m\\
		\ac{LArTPC} (active) mass & 85 tons\\
		\hline
		Number of Anode planes & 3\\
		Anode planes spacing& \SI{3}{\milli\meter} \\
		Wire pitch & \SI{3}{\milli\meter}  \\
		Wire type & Stainless Steel, $d = \SI{150}{\micro\meter}$ \\
		Wire coating & \SI{2}{\micro\meter} Cu, \SI{0.1}{\micro\meter} Ag\\
		Design Wire tension & \SI{6.0 \pm 1}{\newton}\\
		$\#$ wires (total) & 8256 \\
		$\#$ Induction0 plane (U) wires & 2400 \\
		$\#$ Induction1 plane (V) wires & 2400 \\
		$\#$ Collection plane (Y) wires & 3456 \\
		Wire orientation (w.r.t. vertical) & \SI{60}{\deg},\SI{-60}{\deg},\SI{0}{\deg} (U,V,Y) \\
		\hline
		Cathode voltage (operational) & \SI{-70}{\kilo\volt} \\
		Bias voltages (U,V,Y) &  \SI{-110}{\volt}, \SI{0}{\volt},  \SI{+230}{\volt} \\
		Drift-field & \SI{273}{\volt/\centi\meter} \\
		\hline
		$\#$ Field-cage steps & 64\\
		Ring-to-ring voltage step & \SI{2}{\kilo\volt}\\
		Field cage ring diameter & \SI{2.54}{\centi\meter} \\
		Field cage ring thickness & \SI{0.51}{\centi\meter} \\
		Ring-to-ring distance (center to center) & \SI{4}{\centi\meter} \\
		Corner curvature radius & \SI{5.24}{\centi\meter} \\
		Support structure material & G-10 \\
		\hline
	\end{tabular}
	\caption[MicroBooNE TPC properties.]{MicroBooNE \ac{LArTPC} design parameters and operating conditions. \cite{DetectorPaper}} 
	\label{tab:tpcparam}
\end{table} 

\begin{figure}[htbp]
	\centering
	\includegraphics[width=\textwidth]{gfx/uboone/cryo_tpc.pdf} 
	\caption[Microboone TPC arrangement in cryostat]{Microboone TPC assembly in cryostat, the anode is situated on the left hand side, 64 field cages span the horizontal axis, reaching to the cathode ot the right hand side.}
	\label{fig:uboonetpc}
\end{figure}

\begin{figure}[htbp]
	\centering
	\includegraphics[width=\textwidth]{gfx/EField/tpc-left.jpg}
	\caption[Photograph of \ac{TPC} assembly]{Photograph of the Microboone \ac{TPC} assembly showing the back of the cathode (1) an the field cage rings (2).}
	\label{fig:uboone_tpc_photo_outside}
\end{figure}

\begin{figure}[htbp]
	\centering
	\includegraphics[width=\textwidth]{gfx/EField/tpc-interior.jpg}
	\caption[Interior photograph of \ac{TPC} assembly]{Interior Photograph of the Microboone \ac{TPC} assembly showing the cathode (1) on the left hand side, the field cage rings (2) in the central region and the wireplane (3) to the right}
	\label{fig:uboone_tpc_photo_inside}
\end{figure}

\section{Light Collection System}
Liquid argon is a bright scintillator (see \autoref{tpc:sci}), sampling the light is essential for 3D event reconstruction and can be used for calorimetry.
The light produced by neutrino interactions in MicroBooNE is an important input for both event selection and reconstruction.  
One of the critical capabilities the light collection system provides is the ability to form a beam-event trigger when a pulse of light is observed in coincidence with the beam spill.
Such a trigger will substantially reduce the overall data output rate.
For non-beam physics studies, the light system provides triggering and an event $t_0$ for the \ac{LArTPC} system.   

The light collection system consists of primary and secondary sub-systems. 
The primary light collection system is made up of optical units, each one consisting of a \ac{PMT}\footnote{8-inch diameter Hamamatsu 5912-02MOD cryogenic \ac{PMT}} located behind a wavelength-shifting plate.
In total, 32 optical units were installed, yielding 0.9\% photocathode coverage.
The secondary system consists of four light guide paddles.
These paddles were introduced for R\&D studies for future \ac{LArTPC}, and are placed near the primary optical units to allow a comparison of their performances. A flasher system, used for calibration, consists of optical fibers bringing visible light from an LED to each \ac{PMT} face.

The light collection detectors are located in the $y$-$z$ plane behind the anode planes of the \ac{LArTPC}. The combined transparency of the three anode planes is 86\% for light at normal incidence. This transparency value assumes 100\% of VUV photons impinging on the wires are absorbed. The detectors were placed so as not to be obscured by the \ac{LArTPC} structural cross-bars.

\section{Readout and Data Acquisition}
Both, the 8256 sense wires and the 32 \acp{PMT} produce analog signals that must be amplified, digitized and stored for later analysis. 
While the \ac{LArTPC} and \ac{PMT} readout systems share the same back-end design that organizes and packages the data for delivery to the \ac{DAQ} system, each of them employ different analog front-end and digitization designs.

To obtain optimum detector performance, MicroBooNE uses cryogenic low-noise front-end electronics for readout of the sense wires.
An ASIC\footnote{Application-specific integrated circuit} containing a preamplifier, shaper, and signal driver is connected to each sense wire as close as possible on the wire carrier boards, so reducing total input impedance. 
The ASIC was custom designed for the use in \acp{LArTPC} \cite{readoutelec}.
Individual wires are in addition connected to the wire bias voltage distribution system, decoupling
capacitors, and calibration networks.
The output signal of the ASIC is then brought outside the cryostat trough a feedthrough hosting a custom designed intermediate amplifiers.
The amplified warm signal is then routed to an \ac{ADC} module\footnote{each module hosts eight AD9222 octal-channel 12-bit ADCs} situated in a standard rack on the MicroBooNE platform above the detector.
The ADC module digitizes the signals continuously at 16 MHz.
Each channel has a configurable baseline, which is either set low (450 ADC counts) for collection channels or at the middle of the dynamic range (2055 ADC counts) for induction channels.
This configuration ensures that both the collection plane unipolar signals and the induction plane bipolar signals can make optimal use of the ADCs dynamic range.
Behind the ADCs a FPGA\footnote{Field programmable gate array} is responsible for data processing and reduction (lossless Huffman encoding), as well as preparation for readout by the \ac{DAQ}.
One FPGA handles 64 digitized channels and employs a circular DRAM\footnote{Dynamic random-access memory} buffer.

The signal path of the \acp{PMT} is considerably less complex compared to the sense wires. 
The \ac{PMT} itself produces a large enough signal that is able to overcome the distance to the cryostat volume, hence no cold preamplification is necessary.
From each \ac{PMT} a low-gain and high-gain is extracted and wired to a shaper and pre-amlifier situated outside the cryostat. 
The pre-processed analog signal is then digitized with at a sampling rate of \SI{64}{\mega\hertz} by a 12-bit \ac{ADC}\footnote{ADS5272 octal-channel 12-bit ADC}.

The data from each crate (hosting either \ac{TPC} or \ac{PMT} \acp{ADC}) of the backend electronics is sent to a dedicated server via optical fibers.
A real-time application places this data in an internal buffer, collects all segments belonging to an event (from all crates), and creates a sub-event fragment.
The beam-triggered data readout stream, in which the data arrives with every trigger, these fragments are sent to a single event-building machine over an internal network. 
Full events are checked for consistency and written to local disk on the event builder before being sent offline for further processing and storage.
A high-level software trigger, is applied to the data to determine whether events should be written locally or ignored. 
\begin{figure}[htbp]
	\centering
	\includegraphics[width=\textwidth]{gfx/uboone/MicroBooNEReadoutScheme.pdf} 
	\caption[MicroBooNE readout schematic]{Schematic path of the \ac{TPC} and \ac{PMT} readout front- and back-end. Analog signals are generated on the right hand side and evolve to the right trough different amplification, shaping and digitization steps. Adapted from \cite{DetectorPaper}.}
	\label{fig:ubreadout}
\end{figure}







