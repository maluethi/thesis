import matplotlib.pyplot as plt
import argparse
import numpy as np


parser = argparse.ArgumentParser(description='Plotting Time Histograms')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
args = parser.parse_args()


# Get the data
df = np.genfromtxt('data_nist.txt')

rho_86 = 1 / 1.403

energy = df[:, 0]
photoeff = df[:,2] * rho_86
pair = df[:, 3] + df[:, 4] * rho_86
compton = df[:, 1] * rho_86

tot = photoeff + pair + compton

plt.style.use('../mythesis.mplstyle')

fig, ax = plt.subplots(1, 1)

ax.loglog(energy, photoeff, '--', label='photoelectric effect',)
ax.loglog(energy, pair, '--', label='pair production')
ax.plot(energy, compton, '--',  label='compton scattering')
ax.plot(energy, tot, label='total attenuation')


ax.set_xlim([1E-3, 1E3])
ax.set_ylim([1E-4, 1E4])


ax.set_xlabel('Photon Energy [MeV]')
ax.set_ylabel('Absorbtion Coefficient [cm]')


ax.legend()

ax.grid(True,which="both",ls="-")

plt.tight_layout()

if not args.prod:
    plt.show()
else:
    plt.savefig('../../LArTPC/gamma_ray_abs.pdf')