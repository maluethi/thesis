import matplotlib.pyplot as plt
import argparse
import numpy as np
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-i', dest='id', type=int, default=2, help='laser system identifier')

args = parser.parse_args()
plt.style.use('../mythesis.mplstyle')


filename = 'time-mapping.npy'

data = np.load(filename)

laser_times = data[0]
tpc_times = data[1]
delta = data[2][1:-100] * 1000

laser_diff = np.diff(laser_times[1])
tpc_diff = np.diff(tpc_times[1])

fig = plt.figure(0)

gs = gridspec.GridSpec(3, 3)
gs.update(left=0.1, right=0.95, top=0.95, bottom=0.1, hspace=0.3)
ax1 = plt.subplot(gs[0:2, :])
ax_time = plt.subplot(gs[2, 0:2])
ax_hist = plt.subplot(gs[2, 2])

w, h = fig.get_size_inches()
fig.set_size_inches(w, 7)

ax1.plot(laser_diff, label='laser')
ax1.plot(tpc_diff, label='tpc')
ax1.set_xlim([0, len(delta)])
ax1.set_ylim([0, 15])
ax1.set_xlabel(r'Event Number')
ax1.set_ylabel(r'$t_{i+1} - t_{i}[s]$')
ax1.grid(True ,which="both",ls="-")
ax1.legend()


limzoom = [17525, 17600]
ax2 = plt.axes([0,0,1000,10000])
# Manually set the position and relative size of the inset axes within ax1
ip = InsetPosition(ax1, [0.1,0.55,0.5,0.4])
ax2.set_axes_locator(ip)
ax2.plot(np.arange(limzoom[0],limzoom[1],1), laser_diff[limzoom[0]:limzoom[1]])
ax2.plot(np.arange(limzoom[0],limzoom[1],1), tpc_diff[limzoom[0]:limzoom[1]])
ax2.set_ylim([0, 8])
ax2.set_xlim(limzoom)


#mark_inset(ax1, ax2, loc1=2, loc2=4, fc="none", ec='0.5')

ax_time.plot(delta, '+', markersize='0.5')
ax_time.set_xlim([0, len(delta)])
ax_time.set_ylim([-2, 12])
ax_time.set_xlabel(r'Event Number')
ax_time.set_ylabel(r'$t_{Laser} - t_{TPC}[ \mu s]$')
ax_time.grid(True,which="both",ls="-")

ax_hist.hist(delta[:-100], bins=np.arange(-2,12.2,0.25), orientation='horizontal')
ax_hist.set_xlabel('Counts')
ax_hist.grid(True ,which="both",ls="-")
ax_hist.set_ylim([-2, 12])
ax_hist.get_yaxis().set_ticklabels([])

if not args.prod:
    plt.show()
else:
    plt.savefig('../../LCS-System/time-map.pdf')