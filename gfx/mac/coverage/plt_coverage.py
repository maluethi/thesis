__author__ = 'matthias'

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.collections import LineCollection
from matplotlib.collections import PatchCollection
from matplotlib.patches import Circle, Rectangle
import argparse

parser = argparse.ArgumentParser(description='Plotting Time Histograms')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
args = parser.parse_args()

linear = lambda m, x, x1, y1 : m * (x - x1) + y1

def calcTangentAngleGeneric(LineOrigin, CircleCenter, CircleRadius):
    """ This function calculates the two angles under which a line starting at LineOrigin touches a circle.
    Input:
     - LineOrigin: [x y]
     - CircleCenter: [x y]
     - CircleDiameter: r
     Output:
      [higherAngle lowerAngle] in degrees """
    distance = np.sqrt(np.sum((LineOrigin - CircleCenter)**2))
    angle_touching = np.degrees(np.arcsin(CircleRadius/distance))
    angle_to_ring = calcAngle(LineOrigin, CircleCenter)
    touchingAngles = np.hstack([angle_to_ring - angle_touching, angle_to_ring + angle_touching])
    return touchingAngles

def calcAngle(LineOrigin, CircleCenter):
    """ Calculate the angle between two points in the cartesian coordinate system in degrees """
    deltaXY = (LineOrigin - CircleCenter)
    angle = np.arctan(deltaXY[1]/deltaXY[0])
    return np.degrees(angle)

def calcTangentAngles(LineOrigin, CircleCenter, CircleRadius):
    """ Returns an array of angle pairs under which a line going trough LineOrigin touches circle(s) with center at
    CircleCenter and a radius of CircleRadius """
    LineOrigin = np.array(LineOrigin)
    CircleCenter = np.array(CircleCenter)

    nRings = CircleCenter.shape[0]
    angles = np.zeros([nRings, 2])

    for i in range(nRings):
        angles[i, :] = calcTangentAngleGeneric(LineOrigin, CircleCenter[i, :], CircleRadius)

    return angles


def findView(touchingAngles):
    """" Returns an array of True or False values: True if there is a gap to view trough and False if there is no gap"""
    touchingAngles = np.array(touchingAngles)
    n_rings = touchingAngles.shape[0]

    view = np.zeros([n_rings-1])
    for i in range(0, n_rings-1):
        openingAngle = touchingAngles[i, 1] - touchingAngles[i, 0]
        if openingAngle > 0.:
            view[i] = 1
    return view

def getOpeningAngle(viewableAngles):
    """" Calculates the opening angle of an array of angle pairs and returns them as a list  """
    openingAngle = []
    for i in range(viewableAngles.shape[0]):
        openingAngle.append(viewableAngles[i, 1] - viewableAngles[i, 0])

    openingAngle = np.array(openingAngle)
    return openingAngle

def calcTPCIntersectionPoints(LaserPosition, LaserAngles, endpoint):

    m = np.tan(np.radians(LaserAngles)).flatten()

    n_angles = m.shape[0]
    intersection = linear(m, endpoint, LaserPosition[0], LaserPosition[1])
    x = np.ones([n_angles, 1])*endpoint

    intersectionXY = np.vstack([x.flatten(), intersection])

    return intersectionXY

def produceLaserTracks(trackResolution, viewableAngles, start=0, stop=99):

    openingAngle = getOpeningAngle(viewableAngles)

    if stop == 99:
        stop = len(openingAngle)

    shotsInOpening = np.floor(openingAngle/trackResolution)
    stepsInOpeining = openingAngle/(shotsInOpening+1)
    tracks = []
    for i in range(start, stop):
        for k in range(int(shotsInOpening[i])):
            tracks.append(viewableAngles[i, 0] + (k+1)*stepsInOpeining[i])


    return np.array(tracks)

def plotRings(ax, RingPositions, RingDiameter):

    n_rings = RingPositions.shape[0]
    circles = []
    for i in range(n_rings):
        circle = Circle(RingPositions[i,:], RingDiameter)
        circles.append(circle)

    RingCollection = PatchCollection(circles, color='black')
    ax.add_collection(RingCollection, )

def plotBar(ax):
    tpc_length = 1036
    ring_diameter = 2.5
    ring_radius = ring_diameter / 2

    tpc_height = 233

    ubar = Rectangle((0 - ring_radius, -tpc_height/2), ring_radius, tpc_height)
    dbar = Rectangle((tpc_length - ring_radius, -tpc_height/2), ring_radius, tpc_height)
    RingCollection = PatchCollection([ubar, dbar], color='black')
    ax.add_collection(RingCollection, )

def plotLaserTracks(ax, LaserPosition, LaserAngles, intersection):
    lines = []
    intersectionPointsEnd = calcTPCIntersectionPoints(LaserPosition, LaserAngles, intersection)
    n_angles = intersectionPointsEnd.shape[1]
    for i in range(n_angles):
        lines.append([LaserPosition, intersectionPointsEnd[:, i]])

    LaserTracksCollection = LineCollection(lines)
    ax.add_collection(LaserTracksCollection)


# basic TPC definitions
ring_diameter = 2.5
ring_radius = ring_diameter/2
center_to_center = 4
gap = center_to_center - ring_diameter
tpc_length = 1036
tpc_width = 256
tpc_height = 233

# defining the field cage rings
n_fieldcages = tpc_width/center_to_center

RingPositionsX = np.zeros([1, 64])
RingPositionsY = np.linspace(1, 64, 64)*center_to_center - gap + 0.25
RingPositionsUpstream = np.vstack([RingPositionsX, RingPositionsY]).transpose()
RingPositionsDownstream = np.vstack([RingPositionsX, RingPositionsY]).transpose() + [tpc_length, 0]

# defining the positi9on of the laser
LaserPositionUpstreamZ = -32
LaserPositionUpstreamX = 109.3
LaserPositionUpstream = np.hstack([LaserPositionUpstreamZ, LaserPositionUpstreamX])

LaserPositionDownstreamX = 40.68
LaserPositionDownstreamY = 106.53135477799671
LaserPositionDownstream = np.hstack([LaserPositionDownstreamX, LaserPositionDownstreamY]) + [tpc_length, 0]

# calculations for the upstream laser mirror
anglesUpstream = calcTangentAngles(LaserPositionUpstream, RingPositionsUpstream, ring_radius)
anglesUpstream = np.roll(anglesUpstream, 1)
viewableUpstream = findView(anglesUpstream)
viewableAnglesUpstream = anglesUpstream[np.flatnonzero(viewableUpstream)]

# calculations for the upstream laser mirror
anglesDownstream = calcTangentAngles(LaserPositionDownstream, RingPositionsDownstream, ring_radius)
anglesDownstream = np.roll(np.fliplr(anglesDownstream), 1)
viewableDownstream = np.logical_not(findView(anglesDownstream))
viewableAnglesDownstream = np.fliplr(anglesDownstream[np.flatnonzero(viewableDownstream)])

# laser angles generation
trackResolution = 0.1

laser_tracks_upstream = produceLaserTracks(trackResolution, viewableAnglesUpstream + 360, )
laser_tracks_downstream = produceLaserTracks(trackResolution, viewableAnglesDownstream + 180)

plt.style.use('../mythesis.mplstyle')

fig, (ax1, ax2) = plt.subplots(2,1, sharex=True )

w, h = fig.get_size_inches()
fig.set_size_inches(w, 4)

plotRings(ax1, RingPositionsUpstream, ring_radius)
plotRings(ax1, RingPositionsDownstream, ring_radius)
#plotLaserTracks(ax, LaserPositionUpstream, viewableAnglesUpstream, 1000)
#plotLaserTracks(ax, LaserPositionDownstream, viewableAnglesDownstream, 0)
plotLaserTracks(ax1, LaserPositionUpstream, laser_tracks_upstream, tpc_length)
plotLaserTracks(ax1, LaserPositionDownstream, laser_tracks_downstream, 0)



ax1.set_xlim(-50, tpc_length + 50)
ax1.set_ylim(0, 256)
ax1.set_ylabel("x [cm]")
ax1.set_title("top view of TPC")

#ax1.set_xticks()
ax1.set_yticks([0, 128,256])



# vertical calculations
tpc_height = 116* 2 #242.6
LaserPositionUpstreamY = tpc_height/2
halfOpeningAngleVertical = np.degrees((np.pi/2 - np.arctan(LaserPositionDownstreamX/(tpc_height/2)))) - 4
verticalTrackRes = 1.2

NstepsVertical = np.floor(halfOpeningAngleVertical) / verticalTrackRes
stepsVertical = np.linspace(-halfOpeningAngleVertical, halfOpeningAngleVertical, NstepsVertical)
plotBar(ax2)

plotLaserTracks(ax2, [LaserPositionUpstream[0], 0], stepsVertical, tpc_length)
plotLaserTracks(ax2, [LaserPositionDownstream[0], 0], stepsVertical+180, 0)


ax2.set_xlim(-50, tpc_length + 50)
ax2.set_ylim(-tpc_height/2, +tpc_height/2)
ax2.set_xlabel("z [cm]")
ax2.set_ylabel("y [cm]")
ax2.set_title("side view of TPC")
ax2.set_yticks([-116, 0, 116])
fig.tight_layout()

if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/coverage.pdf')


