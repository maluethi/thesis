import matplotlib.pyplot as plt
import argparse
import numpy as np


parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-i', dest='id', type=int, default=2, help='laser system identifier')

args = parser.parse_args()
plt.style.use('../mythesis.mplstyle')


filename_mid = 'mid_xy.txt'
filename_sid = 'sid_xy.txt'

data_mid = np.loadtxt(filename_mid, comments='%')
x = data_mid[:, 0]
y = data_mid[:, 1] - 1.213

Ex_mid_x = data_mid[:, 2]
Ey_mid_y = data_mid[:, 3]
Ez_mid_z = data_mid[:, 4]

data_sid = np.loadtxt(filename_sid, comments='%')

Ex_sid_x = data_sid[:, 2]
Ey_sid_y = data_sid[:, 3]
Ez_sid_z = data_sid[:, 4]

E0 = 70000/2.56

Erange_u = 20
Erange_l = -Erange_u
Erange = np.linspace(Erange_l,Erange_u, 41)

max_x = np.argmax(Ex_mid_x)
min_x = np.argmin(Ex_mid_x)
Emax_x = ((Ex_mid_x[max_x] - E0) / E0) * 100
Emin_x = ((Ex_mid_x[min_x] - E0) / E0) * 100

fig, ax = plt.subplots(nrows=3, ncols=2, sharex=True, sharey=True)
ax1, ax2, ax3 = ax[:, 0]
ax1_l, ax2_l, ax3_l = ax[:, 1]

fig.set_size_inches(5.555, 8)
cs1 = ax1_l.tricontourf(x, y, ((Ex_mid_x - E0) / E0) * 100, levels=Erange)
cs2 = ax2_l.tricontourf(x, y, Ez_mid_z / E0 * 100, levels=Erange)
cs3 = ax3_l.tricontourf(x, y, Ey_mid_y / E0 * 100, levels=Erange)
cs1_l = ax1.tricontourf(x, y, ((Ex_sid_x - E0) / E0) * 100, levels=Erange)
cs2_l = ax2.tricontourf(x, y, Ez_sid_z / E0 * 100, levels=Erange)
cs3_l = ax3.tricontourf(x, y, Ey_sid_y / E0 *100, levels=Erange)

ax1_l.set_ylabel(r'$\frac{E_x - E_0}{E_0}$ [\%]')
ax2_l.set_ylabel(r'$\frac{E_y}{E_0}$ [\%]')
ax3_l.set_ylabel(r'$\frac{E_z}{E_0}$ [\%]')
ax1_l.yaxis.set_label_coords(1.1,0.5)
ax2_l.yaxis.set_label_coords(1.1,0.5)
ax3_l.yaxis.set_label_coords(1.1,0.5)

ax1_l.yaxis.set_label_position("right")
ax2_l.yaxis.set_label_position("right")
ax3_l.yaxis.set_label_position("right")

ax1.set_ylabel('y [cm]')
ax2.set_ylabel('y [cm]')
ax3.set_ylabel('y [cm]')

ax1.set_xticks([0.01, 1.28, 2.55])
ax1.set_xticklabels([0, 116, 256])

ax3.set_xlabel('x [cm]')
ax3_l.set_xlabel('x [cm]')

cbar_ax = fig.add_axes([0.125, 0.07, 0.775, 0.015])
cb = fig.colorbar(cs1, cax=cbar_ax, orientation='horizontal')
fig.subplots_adjust(bottom=0.15, top=0.95)

cbar_ax.text(0.5, -3, r'Deviation from Nominal [\%]', horizontalalignment='center')

ax1.set_title('Slice at z=0.1m')
ax1_l.set_title('Slice at z=5.1m')

# nasty fix for pdf exporting
for cs in [cs1, cs2, cs3, cs1_l, cs2_l, cs3_l, cbar_ax]:
    for c in cs.collections:
        c.set_edgecolor("face")


for ax in [ax1, ax2, ax3]:
    ax.set_yticks([-1.213, 0, 1.213])
    ax.set_yticklabels([-116, 0, 116])
    ax.set_label('x [cm]')


if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/sim_efield_xy.pdf')