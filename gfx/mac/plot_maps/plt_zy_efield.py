import root_numpy as rn
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from collections import namedtuple
import larana.lar_utils as laru
import argparse

parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-d', dest='file', type=str, default='', help='file to process')
parser.add_argument('-s', dest='slice', type=int, default=13, help='slice')

args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

name = args.file.split('/')[-1][:2]
sim_filename = './data/EMap-Data.root'

dist_sim = laru.get_histos(sim_filename, dataproduct='emap')

simulation = laru.make_array(dist_sim).view(np.recarray)

x, y, z = np.meshgrid(np.linspace(laru.TPC.x_min, laru.TPC.x_max, simulation.shape[0]),
                      np.linspace(laru.TPC.y_min, laru.TPC.y_max, simulation.shape[1]),
                      np.linspace(laru.TPC.z_min, laru.TPC.z_max, simulation.shape[2]))
dimens = {0: 'dx',
          1: 'dy',
          2: 'dz', }

slice = args.slice
f, axes = plt.subplots(3, 1, sharey=True, sharex=True)
w, h = f.get_size_inches()
f.set_size_inches(w, 7)

axes[0].set_title("y={:.1f} [cm]".format(slice * 2 * 126 / 26 - 126))
ax_im = []

shape = simulation.shape
shape_x = shape[0]
shape_y = shape[1]
shape_z = shape[2]

for dim, ax in enumerate(axes):
    siml = simulation[:, slice, :]
    ax.set_xlim([0.5, shape_z-0.5])
    ax.set_ylim([0.5, shape_x-0.5])
    ax.set_xticks([1, shape_z/2, shape_z])
    ax.set_yticks([0.5, shape_x-0.5])
    ax.set_yticklabels([0, 256])
    ax.set_xticklabels([0, 500, 1035])


    limits_true = {0: [-20., 20.],
                   1: [-20, 20],
                   2: [-20, 20]}

    limits = limits_true

    data = siml[dimens[dim]]

    data[np.abs(data) > 1E8] = np.nan
    if dim == 0:
        data = (data - 0.273) / 0.273 * 100
    else:
        data = data / 0.273 * 100

    im = ax.imshow(data[:, :], cmap=cm.viridis, interpolation='nearest', aspect='auto', vmin=limits[dim][0],
                        vmax=limits[dim][1])
    ax_im.append(im)
    ax.set_ylabel('x [cm]')
    #ax.invert_yaxis()

axes[2].set_xlabel('z [cm]')

axes[0].text(shape_z + 2, shape_x/2, r'$\frac{E_x - E_0}{E_0}$ [\%]', rotation=90, ha="left", va="center" )
axes[1].text(shape_z + 2, shape_x/2, r'$\frac{E_y}{E_0}$ [\%]', rotation=90, ha="left", va="center" )
axes[2].text(shape_z + 2, shape_x/2, r'$\frac{E_z}{E_0}$ [\%]', rotation=90, ha="left", va="center" )

cbar_ax = f.add_axes([0.125, 0.07, 0.775, 0.015])
cb = f.colorbar(ax_im[1], cax=cbar_ax, orientation='horizontal')

f.subplots_adjust(bottom=0.15, top=0.95)

cbar_ax.text(0.5, -3, r'Deviation from Nominal [\%]', horizontalalignment='center')


#axes[0].twinx().yaxis.set_label_position("right")
#axes[1].twinx().yaxis.set_label_position("right")
#axes[2].twinx().yaxis.set_label_position("right")


#f.tight_layout()
#f.savefig("./output/slice_zy/sclice-{}.png".format(sl), bbox_inches=0, transparent=True)

if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/efield_zx-{}.pdf'.format(slice))