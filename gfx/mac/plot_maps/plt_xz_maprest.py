import root_numpy as rn
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from collections import namedtuple
import larana.lar_utils as laru
import argparse

parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-d', dest='file', type=str, default='', help='file to process')

args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

name = args.file.split('/')[-1][:2]
sim_filename = './data/SpaceCharge.root'
rec_filename = '/home/data/uboone/laser/processed/maps/smooth/RecoCorr-N3-S50-toyMC-2side-Anode-new.root'

dist_sim = laru.get_histos(sim_filename)
dist_rec = laru.get_histos(rec_filename)

simulation = laru.make_array(dist_sim).view(np.recarray)
recovered = laru.make_array(dist_rec).view(np.recarray)



x, y, z = np.meshgrid(np.linspace(laru.TPC.x_min, laru.TPC.x_max, simulation.shape[0]),
                      np.linspace(laru.TPC.y_min, laru.TPC.y_max, simulation.shape[1]),
                      np.linspace(laru.TPC.z_min, laru.TPC.z_max, simulation.shape[2]))


slices = [10, 50]
f, axes = plt.subplots(3, 2, sharey=True, sharex=True)
w, h = f.get_size_inches()
f.set_size_inches(w, 7)

ax1 = axes[:, 0]
ax2 = axes[:, 1]

for sl, ax in zip(slices, [ax1, ax2]):
    print(sl)
    #
    siml = simulation[:, :, sl]
    reco = recovered[:, :, sl]

    dimens = {0: 'dx',
              1: 'dy',
              2: 'dz',}

    limits_true = {0: [-10., 10.],
              1: [-20, 20],
              2: [-6, 6]}

    lim = 4
    limits_sim = {0: [-lim, lim],
                  1: [-lim, lim],
                  2: [-lim, lim]}

    limits = limits_true
    ax[0].set_title("z={:.1f} [cm]".format(sl * 1036/101))

    for dim in range(3):
        #qu = ax.contourf(z[sl, :, :], x[sl, :, :], dist.dx, cmap=cm.Spectral, vmin=-20, vmax=20, interpolation=None)
        #qu.cmap.set_over('#FFFFFF')

        sim = siml[dimens[dim]].T
        rec = reco[dimens[dim]].T

        res = (- rec)

        im = ax[dim].imshow(res, cmap=cm.Spectral, vmin=limits[dim][0], vmax=limits[dim][1], interpolation='nearest',
                            aspect='auto')

        res[res > 10000] = np.nan

        mx = np.nanmax(res)
        mx_loc =  np.unravel_index(np.nanargmax(res, axis=None), res.shape)
        mn = np.nanmin(res)
        mn_loc = np.unravel_index(np.nanargmin(res, axis=None), res.shape)



        lval = res[20,4]
        uval = res[20,-4]
        print(res.shape)
        print(lval, uval)

        #ax[dim].plot(mn_loc[1], mn_loc[0], 'v', c='black', markersize=5)
        #ax[dim].plot(mx_loc[1], mx_loc[0], '^', c='black', markersize=5)
        #ax[dim].text(mn_loc[1]-4, mn_loc[0], '{:03.1f}'.format(mn))
        #ax[dim].text(mx_loc[1]-4, mx_loc[0], '{:03.1f}'.format(mx))

        print(mx, mx_loc, mn, mn_loc)

        im.cmap.set_over('#FFFFFF')
        im.cmap.set_under('#FFFFFF')

        if sl == slices[-1]:
            divider = make_axes_locatable(ax[dim])
            cax = divider.append_axes("right", size="2%", pad=0.05)
            cax.set_title("{} [cm]".format(dimens[dim]))
            plt.colorbar(im, cax)
            ax[dim].invert_yaxis()

        ax[dim].set_xlim([0, 25])
        ax[dim].set_ylim([0, 25])

        ax[dim].set_xticks([0, 12, 25])
        ax[dim].set_yticks([0, 12, 25])
        ax[dim].set_yticklabels([-116, 0, 116])
        ax[dim].set_xticklabels([0, 128, 256])


for ax in ax1:
    ax.set_ylabel('y [cm]')

axes[2,0].set_xlabel('x [cm]')
axes[2,1].set_xlabel('x [cm]')



f.tight_layout()
#f.savefig("./output/slice_zy/sclice-{}.png".format(sl), bbox_inches=0, transparent=True)

if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/distpl_xz-{}.pdf'.format(name))