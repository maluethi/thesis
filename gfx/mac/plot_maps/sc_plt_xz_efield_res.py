import root_numpy as rn
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from collections import namedtuple
import larana.lar_utils as laru
import argparse

parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-d', dest='file', type=str, default='', help='file to process')
parser.add_argument('-s', dest='slice', type=int, default='', help='file to process')

args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

name = args.file.split('/')[-1][:2]
sim_filename = './data/Emap-Calcu26101-Simu.root'
#sim_filename = './data/Emap-NTT-1-N3-S50-LaserMC-2sideAnode-DispAddress.root'
rec_filename = './data/SpaceCharge-E273.root'

dist_sim = laru.get_histos(sim_filename, dataproduct='emap')
dist_rec = laru.get_histos(rec_filename, dataproduct='secret')

simulation = laru.make_array(dist_sim).view(np.recarray)
recovered = laru.make_array(dist_rec).view(np.recarray)

x, y, z = np.meshgrid(np.linspace(laru.TPC.x_min, laru.TPC.x_max, simulation.shape[0]),
                      np.linspace(laru.TPC.y_min, laru.TPC.y_max, simulation.shape[1]),
                      np.linspace(laru.TPC.z_min, laru.TPC.z_max, simulation.shape[2]))

print(simulation.shape)

slices = [args.slice, args.slice+10]
#slices = [10, 40]
f, axes = plt.subplots(3, 2, sharey=True, sharex=True)
w, h = f.get_size_inches()
f.set_size_inches(w, 7)

ax1 = axes[:, 0]
ax2 = axes[:, 1]

for sl, ax in zip(slices, [ax1, ax2]):
    print(sl)
    #
    siml = simulation[:, :, sl]
    reco = recovered[:, :, sl]

    dimens = {0: 'dx',
              1: 'dy',
              2: 'dz',}

    limits_true = {0: [-20., 20.],
              1: [-20, 20],
              2: [-20, 20]}

    limits_abs = {0: [200, 310],
                  1: [-30, 30],
                  2: [-30, 30]}

    limits = limits_abs

    ax[0].set_title("z={:.1f} [cm]".format(sl * 1036/80))

    for dim in range(3):


        sim = siml[dimens[dim]].T
        rec = reco[dimens[dim]].T*1000



        sim[np.abs(sim) > 1E8] = np.nan
        rec[np.abs(rec) > 1E8] = np.nan


        #if dim == 0:
        #    rec = -rec

        res = sim - rec
        im = ax[dim].imshow(rec[:,:] , cmap=cm.viridis, interpolation=None, aspect='auto', vmin=limits[dim][0], vmax=limits[dim][1])



        #mx = np.nanmax(res)
        #mx_loc = np.unravel_index(np.nanargmax(res, axis=None), rec.shape)
        #mn = np.nanmin(res)
        #mn_loc = np.unravel_index(np.nanargmin(res, axis=None), rec.shape)

        print(rec.shape)
        #ax[dim].plot(mn_loc[1], mn_loc[0], 'v', c='black', markersize=5)
        #ax[dim].plot(mx_loc[1], mx_loc[0], '^', c='black', markersize=5)
        #ax[dim].text(mn_loc[1]-4, mn_loc[0], '{:03.1f}'.format(mn))
        #ax[dim].text(mx_loc[1]-4, mx_loc[0], '{:03.1f}'.format(mx))

        #print(mx, mx_loc, mn, mn_loc)

        im.cmap.set_over('#FFFFFF')
        im.cmap.set_under('#FFFFFF')

        if sl == slices[-1]:
            divider = make_axes_locatable(ax[dim])
            cax = divider.append_axes("right", size="2%", pad=0.05)
            cax.set_title("{} [cm]".format(dimens[dim]))
            plt.colorbar(im, cax)
            ax[dim].invert_yaxis()




for ax in ax1:
    ax.set_ylabel('y [cm]')

axes[2,0].set_xlabel('x [cm]')
axes[2,1].set_xlabel('x [cm]')
plt.show()


f.tight_layout()
#f.savefig("./output/slice_zy/sclice-{}.png".format(sl), bbox_inches=0, transparent=True)

if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/distpl_xz-{}.pdf'.format(name))