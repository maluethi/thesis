import root_numpy as rn
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from collections import namedtuple
import larana.lar_utils as laru
import argparse

parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-s', dest='sl', type=int, default=13, help='laser system identifier')
parser.add_argument('-d', dest='file', type=str, default='', help='file to process')

args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

sim_filename = args.file

dist_sim = laru.get_histos(sim_filename)

simulation = laru.make_array(dist_sim).view(np.recarray)

x, y, z = np.meshgrid(np.linspace(laru.TPC.x_min, laru.TPC.x_max, simulation.shape[0]),
                      np.linspace(laru.TPC.y_min, laru.TPC.y_max, simulation.shape[1]),
                      np.linspace(laru.TPC.z_min, laru.TPC.z_max, simulation.shape[2]))


sl = args.sl
f, ax = plt.subplots(3, 1, sharey=True, sharex=True)
w, h = f.get_size_inches()

if sl == 5:
    f.set_size_inches(w, 6)
else:
    f.set_size_inches(w, 7)

print(sl)
print(simulation.shape)

siml = simulation[sl, :, :]
print(siml.shape)

dimens = {0: 'dx',
          1: 'dy',
          2: 'dz',}

limits_true = {0: [-10., 10.],
          1: [-20, 20],
          2: [-15, 15]}

limits_sim = {0: [-1, 1],
              1: [-1, 1],
              2: [-1, 1]}

limits = limits_true

ax[0].set_title("x={:.1f} [cm]".format(sl * 256/26))

for dim in range(3):

    data =siml[dimens[dim]]


    im = ax[dim].imshow(data , cmap=cm.Spectral, vmin=limits[dim][0], vmax=limits[dim][1], interpolation=None,
                        aspect='auto')

    data[data > 10000] = np.nan

    mx = np.nanmax(data)
    mx_loc =  np.unravel_index(np.nanargmax(data, axis=None), data.shape)
    mn = np.nanmin(data)
    mn_loc = np.unravel_index(np.nanargmin(data, axis=None), data.shape)



    lval = data[20,4]
    uval = data[20,-4]
    print(data.shape)
    print(lval, uval)

    #ax[dim].plot(mn_loc[1], mn_loc[0], 'v', c='black', markersize=5)
    #ax[dim].plot(mx_loc[1], mx_loc[0], '^', c='black', markersize=5)
    #ax[dim].text(mn_loc[1]-4, mn_loc[0], '{:03.1f}'.format(mn))
    #ax[dim].text(mx_loc[1]-4, mx_loc[0], '{:03.1f}'.format(mx))

    print(mx, mx_loc, mn, mn_loc)

    im.cmap.set_over('#FFFFFF')
    im.cmap.set_under('#FFFFFF')

    divider = make_axes_locatable(ax[dim])
    cax = divider.append_axes("right", size="2%", pad=0.05)
    cax.set_title("{} [cm]".format(dimens[dim]))
    plt.colorbar(im, cax)
    ax[1].invert_yaxis()

    ax[dim].set_xlim([0, 100])
    ax[dim].set_ylim([0, 25])

    ax[dim].set_xticks([0, 50, 100])
    ax[dim].set_yticks([0, 12, 25])
    ax[dim].set_yticklabels([-116, 0, 116])
    ax[dim].set_xticklabels([0, 500, 1035])
    ax[dim].set_ylabel('y [cm]')



ax[2].set_xlabel('z [cm]')



f.tight_layout()
if not args.prod:
    plt.show()
else:
    plt.savefig('../../AP_displ/distpl_zy-{}.pdf'.format(args.sl))