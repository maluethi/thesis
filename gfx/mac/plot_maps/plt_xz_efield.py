import root_numpy as rn
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from collections import namedtuple
import larana.lar_utils as laru
import argparse

parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-d', dest='file', type=str, default='', help='file to process')

args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

name = args.file.split('/')[-1][:2]
sim_filename = './data/EMap-Data-NEW.root'

dist_sim = laru.get_histos(sim_filename, dataproduct='emap')

simulation = laru.make_array(dist_sim).view(np.recarray)

x, y, z = np.meshgrid(np.linspace(laru.TPC.x_min, laru.TPC.x_max, simulation.shape[0]),
                      np.linspace(laru.TPC.y_min, laru.TPC.y_max, simulation.shape[1]),
                      np.linspace(laru.TPC.z_min, laru.TPC.z_max, simulation.shape[2]))

print(simulation.shape)


for sl in range(10,50):
    slices = [sl, sl + 50]
    #slices = [10, 40]
    f, axes = plt.subplots(3, 2, sharey=True, sharex=True)
    w, h = f.get_size_inches()
    f.set_size_inches(w, 7)

    ax1 = axes[:, 0]
    ax2 = axes[:, 1]

    for sl, ax in zip(slices, [ax1, ax2]):
        print(sl)
        #
        siml = simulation[:, :, sl]

        dimens = {0: 'dx',
                  1: 'dy',
                  2: 'dz',}

        limits_true = {0: [-20., 20.],
                  1: [-20, 20],
                  2: [-20, 20]}

        limits_sim = {0: [-1, 1],
                      1: [-1, 1],
                      2: [-1, 1]}

        limits = limits_true

        ax[0].set_title("z={:.1f} [cm]".format(sl * 1036/80))

        for dim in range(3):
            ax[dim].set_xlim([0.5, 19.5])
            ax[dim].set_ylim([0.5, 19.5])
    #
            ax[dim].set_xticks([0.5, 10, 19.5])
            ax[dim].set_yticks([0.5, 10, 19.5])
    #
            ax[dim].set_yticklabels([-116, 0, 116])
            ax[dim].set_xticklabels([0, 128, 256])

            data = siml[dimens[dim]].T
            data[data > 1E8] = np.nan
            if dim == 0:
                data = (data - 0.273)/0.273 * 100
            else:
                data = -data / 0.273 * 100

            im = ax[dim].imshow(data[:,:] , cmap=cm.viridis, interpolation=None, aspect='auto', vmin=limits[dim][0], vmax=limits[dim][1])



            mx = np.nanmax(data)
            mx_loc =  np.unravel_index(np.nanargmax(data, axis=None), data.shape)
            mn = np.nanmin(data)
            mn_loc = np.unravel_index(np.nanargmin(data, axis=None), data.shape)

            print(data.shape)
            #ax[dim].plot(mn_loc[1], mn_loc[0], 'v', c='black', markersize=5)
            #ax[dim].plot(mx_loc[1], mx_loc[0], '^', c='black', markersize=5)
            #ax[dim].text(mn_loc[1]-4, mn_loc[0], '{:03.1f}'.format(mn))
            #ax[dim].text(mx_loc[1]-4, mx_loc[0], '{:03.1f}'.format(mx))

            print(mx, mx_loc, mn, mn_loc)

            im.cmap.set_over('#FFFFFF')
            im.cmap.set_under('#FFFFFF')

            if sl == slices[-1]:
                divider = make_axes_locatable(ax[dim])
                cax = divider.append_axes("right", size="2%", pad=0.05)
                cax.set_title("{} [cm]".format(dimens[dim]))
                plt.colorbar(im, cax)
                ax[dim].invert_yaxis()




    for ax in ax1:
        ax.set_ylabel('y [cm]')

    axes[2,0].set_xlabel('x [cm]')
    axes[2,1].set_xlabel('x [cm]')
    plt.show()


f.tight_layout()
#f.savefig("./output/slice_zy/sclice-{}.png".format(sl), bbox_inches=0, transparent=True)

if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/distpl_xz-{}.pdf'.format(name))