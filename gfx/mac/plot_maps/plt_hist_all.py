import root_numpy as rn
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from collections import namedtuple
import larana.lar_utils as laru
import argparse
import itertools
from collections import OrderedDict
parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-t', dest='type', type=str, default='t', help='specify which dimension')

args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

sim_filename = './data/SpaceCharge.root'

if args.type == 't':
    rec_filename = './data/RecoCorr-N3-S50-toyMC-2side-Anode-new.root'
elif args.type == 's':
    rec_filename = './data/TrueDist-N3-S50-broadMuonSim-2side-Anode.root'

dist_sim = laru.get_histos(sim_filename)
dist_rec = laru.get_histos(rec_filename)

simulation = laru.make_array(dist_sim) #.view(np.array)
recovered = laru.make_array(dist_rec) #.view(np.array)


x, y, z = np.meshgrid(np.linspace(laru.TPC.x_min, laru.TPC.x_max, simulation.shape[0]),
                      np.linspace(laru.TPC.y_min, laru.TPC.y_max, simulation.shape[1]),
                      np.linspace(laru.TPC.z_min, laru.TPC.z_max, simulation.shape[2]))


f, axes = plt.subplots(1, 3, sharey=True, sharex=True)
w, h = f.get_size_inches()
f.set_size_inches(w, 2.5)
#

margin = 20
maz_z = 101

z= 1036
stepz = z/maz_z / 1

dims = ['dx', 'dy', 'dz']
sections = ['front-back', 'top-bottom','right-left']

#sec = args.dimension

ranges = {'x': [(slice(1,5), slice(20, 25), slice(5,20)), 3*(slice(1,25),), 3*(slice(10,91),)],
          'y': [3*(slice(1,25),), (slice(1,5), slice(20,25), slice(5,20)), 3*(slice(10,91),)],
          'z': [3*(slice(1,25),), 3*(slice(1,25),), (slice(10,20), slice(81,91), slice(20,81))]}

ranges = OrderedDict(sorted(ranges.items(), key=lambda t: t[0]))

for key, ax, sec in zip(ranges.keys(), axes, sections):
    dsim = [0, 0, 0]
    drec = [0, 0, 0]
    for dim in dims:
        for idx, (rx, ry, rz) in enumerate(zip(*ranges[key])):
            #print(rng, rng[0]*stepz, rng[1]*stepz)
            dsim[idx] += np.power(simulation[dim][rx, ry, rz].flatten(), 2)
            drec[idx] += np.power(recovered[dim][rx, ry, rz].flatten(), 2)

    total = []
    oks = []
    tt = 0
    for idx in range(len(ranges[key])):
        dsim[idx] = np.sqrt(dsim[idx])
        drec[idx] = np.sqrt(drec[idx])

        tot = dsim[idx] - drec[idx]

        ok = np.where((-20 <  tot) & (tot < 20))

        med = np.median(tot[ok])
        std = np.std(tot[ok])

        oks.append(ok)
        total.append(tot[ok])

        print('{}-{}: med: {}, std: {}'.format(sec, idx, med, std))

        ax.hist(tot[ok], bins=61, range=[-3,3],histtype='step', normed=False, stacked=False, log=True),# label=label)
    ax.set_title(sec)
    ax.set_xlabel(r'$\vec{x}_{res}$ [cm]')

    tt = np.concatenate(total)
    print(len(tt))
    med = np.median(tt)
    std = np.std(tt)
    print('tot: med: {}, std: {}'.format( med, std))

    ax.hist(tt, bins=61, range=[-3, 3], histtype='step', normed=False, stacked=False,
                log=True,  color='gray', linestyle='--')


axes[0].set_ylabel(r'counts')
axes[0].set_ylim([1,1E5])
axes[0].set_xlim([-4,4])
axes[0].set_xticks([-4,-2,0,2,4])

f.tight_layout()
if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/histo-ressim-{}-all.pdf'.format(args.type))