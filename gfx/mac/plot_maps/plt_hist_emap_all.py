import root_numpy as rn
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from collections import namedtuple
import larana.lar_utils as laru
import argparse
import itertools
from matplotlib.offsetbox import AnchoredText
from collections import OrderedDict

parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-t', dest='type', type=str, default='t', help='specify which dimension')

args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

sim_filename = './data/EMap-Simu.root'

if args.type == 't':
    rec_filename = './data/EMap-Toy.root'
elif args.type == 's':
    rec_filename = './data/EMap-FullSim.root'

dist_sim = laru.get_histos(sim_filename, dataproduct='emap')
dist_rec = laru.get_histos(rec_filename, dataproduct='emap')

simulation = laru.make_array(dist_sim) #.view(np.array)
recovered = laru.make_array(dist_rec) #.view(np.array)

print(simulation.shape, recovered.shape)

f, axes = plt.subplots(1, 3, sharey=True, sharex=True)
w, h = f.get_size_inches()
f.set_size_inches(w, 3)
#

xmax, ymax, zmax, = simulation.shape

margin = 20
maz_z = zmax

z= 1036
stepz = z/maz_z / 1

dims = ['dx', 'dy', 'dz']
sections = ['front-back', 'top-bottom','right-left']

#sec = args.dimension

# histo config
hist_lim = 12
bins = hist_lim  * 4 + 1
log = False

# slice config
xy_margin = 1
xy_mid = 12
sid_x = int( (xmax - xy_mid - 2*xy_margin) / 2)
z_margin = 2
z_mid = 50
sid_z = int((zmax - z_mid - 2*z_margin) / 2)

xstep = ystep = (slice(xy_margin, sid_x), slice(xmax - sid_x, xmax - xy_margin), slice(sid_x, xmax - sid_x) )
xall = yall = slice(xy_margin, xmax-xy_margin)
zall = slice(z_margin, zmax-z_margin)
zstep = (slice(z_margin, sid_z), slice(zmax - sid_z, zmax - z_margin), slice(sid_z, zmax - sid_z ))



ranges = {'x': [xstep, 3*(yall,), 3*(zall,)],
          'y': [3*(xall,), ystep, 3*(zall,)],
          'z': [3*(xall,), 3*(yall,), zstep]}

ranges = OrderedDict(sorted(ranges.items(), key=lambda t: t[0]))

for key, ax, sec in zip(ranges.keys(), axes, sections):
    dsim = [0, 0, 0]
    drec = [0, 0, 0]
    print(key)
    for dim in dims:
        for idx, (rx, ry, rz) in enumerate(zip(*ranges[key])):
            dsim[idx] += np.power(simulation[dim][rx, ry, rz].flatten(), 2)
            drec[idx] += np.power(recovered[dim][rx, ry, rz].flatten(), 2)

    total = []
    oks = []
    tt = 0
    for idx in range(len(ranges[key])):
        dsim[idx] = np.sqrt(dsim[idx])
        drec[idx] = np.sqrt(drec[idx])

        tot = (dsim[idx] - drec[idx]) / dsim[idx] * 100

        ok = np.where(np.abs(tot) < 20)

        med = np.median(tot[ok])
        std = np.std(tot[ok])

        oks.append(ok)
        total.append(tot[ok])

        print('{}-{}: med: {}, std: {}, '.format(sec, idx, med, std))

        ax.hist(tot[ok], bins=bins, range=[-hist_lim,hist_lim],histtype='step', normed=False, stacked=False, log=log),# label=label)
    ax.set_title(sec)
    ax.set_xlabel(r'$\vec{E}_{res}/\vec{E}_{sim}$ [\%]')

    tt = np.concatenate(total)
    print(len(tt))
    med = np.median(tt)
    std = np.std(tt)
    print('tot: med: {}, std: {}'.format( med, std))

    ax.hist(tt, bins=bins, range=[-hist_lim, hist_lim], histtype='step', normed=False, stacked=False,
                log=log,  color='gray', linestyle='--')


axes[0].set_ylabel(r'counts')
axes[0].set_ylim([1,1E4])
axes[0].set_xlim([-hist_lim,hist_lim])
axes[0].set_xticks([-12,-6,0,6,12])

f.tight_layout()
if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/histo-emap-{}-all.pdf'.format(args.type))