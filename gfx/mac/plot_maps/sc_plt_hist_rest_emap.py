import root_numpy as rn
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from collections import namedtuple
import larana.lar_utils as laru
import argparse
import itertools

parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-d', dest='dimension', type=str, default='', help='specify which dimension')
parser.add_argument('-t', dest='type', type=str, default='t', help='specify which dimension')

args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

sim_filename = './data/Emap-Simu_Cal_mobility.root'

if args.type == 't':
    rec_filename = './data/EMap-Toy.root'
elif args.type == 's':
    rec_filename = './data/TrueDist-N3-S50-broadMuonSim-2side-Anode.root'

dist_sim = laru.get_histos(sim_filename, dataproduct='emap')
dist_rec = laru.get_histos(rec_filename, dataproduct='emap')

simulation = laru.make_array(dist_sim) #.view(np.array)
recovered = laru.make_array(dist_rec) #.view(np.array)


x, y, z = np.meshgrid(np.linspace(laru.TPC.x_min, laru.TPC.x_max, simulation.shape[0]),
                      np.linspace(laru.TPC.y_min, laru.TPC.y_max, simulation.shape[1]),
                      np.linspace(laru.TPC.z_min, laru.TPC.z_max, simulation.shape[2]))


f, axes = plt.subplots(1, 3, sharey=True, sharex=True)
w, h = f.get_size_inches()
f.set_size_inches(w, 2.5)
#

margin = 20
maz_z = 101

z= 1036
stepz = z/maz_z / 1

dims = ['dx', 'dy', 'dz']


xrange = [(1,5),(19,25),(5,19)]
yrange = xrange
zrange = [(10,20),(81,91), (20,81)]

dimens = args.dimension
if dimens == 'z':
    xrng = [(xrange[0][0], xrange[1][1],)]*3
    yrng =xrng
    zrng = zrange
elif dimens == 'x':
    xrng = xrange
    yrng = [(yrange[0][0], xrange[1][1],)]*3
    zrng = [(zrange[0][0], zrange[1][1],)]*3
elif dimens == 'y':
    xrng = [(yrange[0][0], xrange[1][1],)]*3
    yrng = xrange
    zrng = [(zrange[0][0], zrange[1][1],)]*3
else:
    raise ValueError('"{}" dimension is not defined. Valid arguments are x,y,z.'.format(dimens))


total = []
for dim, ax in zip(dims, axes):
    dd = []
    for xr, yr, zr in zip(xrng, yrng, zrng):
        #print(rng, rng[0]*stepz, rng[1]*stepz)
        residuals = (simulation[dim] - recovered[dim])
        dd.append(residuals[xr[0]:xr[1], yr[0]:yr[1], zr[0]:zr[1]].flatten())

    ax.hist(dd, bins=60, range=[-0.1,0.1],histtype='step', normed=False, stacked=False, log=True),# label=label)
    ax.set_xlabel(r'$\Delta {}$ [cm]'.format(dim[-1]))

    for idx, d in enumerate(dd):

        ok = np.where(d < 5)
        d = d[ok]
        med = np.median(d)
        std = np.std(d)

        print('{}-{}: med: {}, std: {}'.format(idx, dim, med, std))

    #ax.grid(True, which="both", ls="-")


#axes[0].set_ylabel(r'counts')
#axes[0].set_ylim([1,1E5])
#axes[0].set_xlim([-4,4])
#axes[0].set_xticks([-4,-2,0,2,4])

f.tight_layout()
if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/histo-ressim-{}-{}.pdf'.format(args.type, dimens))