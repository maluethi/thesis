

import root_numpy as rn
import numpy as np
import matplotlib.pyplot as plt

from matplotlib.backends.backend_pdf import PdfPages

base_dir = "./data/"
filename = "RawData-39501-low.root"
#filename = 'RawData-39501-gauss-hig.root'
raw_file = base_dir + filename

hitfinder = 'gaushit'
classifier = filename[:-5].split('-')[-1]

raw_frames = rn.root2array(raw_file, treename='GetRaw/RawDigit')
hit_frames = rn.root2array(raw_file, treename='GetRaw/HitTree')
#cal_frames = rn.root2array(raw_file, treename='GetRaw/CalTree')

ranges = {'low': 5350,
          'mid': 5800,
          'hig': 5620}

peak_expected = ranges[classifier]
peak_region = 300


np.save('data/raw_low', raw_frames)






