
import root_numpy as rn
import pickle

base_dir = "./data/"
filename = "test.root"
#filename = 'RawData-39501-gauss-hig.root'
raw_file = base_dir + filename

hit_wire = rn.root2array(raw_file, treename='Events',
                  branches='recob::Hits_laserhit__Laser.obj.fWireID.Wire')
hit_plane = rn.root2array(raw_file, treename='Events',
                  branches='recob::Hits_laserhit__Laser.obj.fWireID.Plane')
hit_channel = rn.root2array(raw_file, treename='Events',
                  branches='recob::Hits_laserhit__Laser.obj.fChannel')

ch2wire = {}

for idx, chn in enumerate(hit_channel[0]):
    plane = hit_plane[0][idx]
    wire = hit_wire[0][idx]

    ch2wire.update({chn: (plane, wire)})

with open('ch2wire.txt', "wb") as f:
    pickle.dump(ch2wire, f)