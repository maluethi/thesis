
import root_numpy as rn
import numpy as np
import matplotlib.pyplot as plt
import argparse
from matplotlib import cm


parser = argparse.ArgumentParser(description='Plotting full event hits')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
args = parser.parse_args()
plt.style.use('../mythesis.mplstyle')

base_dir = "./data/"
filename = "test.root"
#filename = 'RawData-39501-gauss-hig.root'
raw_file = base_dir + filename

f, axes = plt.subplots(3, 1,)
f.set_size_inches(5.555, 8)

# We need root_numpy and root
hit_peaktime = rn.root2array(raw_file, treename='Events',
                  branches='recob::Hits_laserhit__Laser.obj.fPeakTime')
hit_start = rn.root2array(raw_file, treename='Events',
                  branches='recob::Hits_laserhit__Laser.obj.fStartTick')
hit_end = rn.root2array(raw_file, treename='Events',
                  branches='recob::Hits_laserhit__Laser.obj.fEndTick')
hit_wire = rn.root2array(raw_file, treename='Events',
                  branches='recob::Hits_laserhit__Laser.obj.fWireID.Wire')
hit_plane = rn.root2array(raw_file, treename='Events',
                  branches='recob::Hits_laserhit__Laser.obj.fWireID.Plane')
evt = 1

u_hits_idx = np.where(hit_plane[evt] == 0)
v_hits_idx = np.where(hit_plane[evt] == 1)
y_hits_idx = np.where(hit_plane[evt] == 2)

planes = ['u', 'v', 'y']

xlims = {0: [0,2383],
        1: [0,2382],
        2: [0,3445]}


for plane, ax in zip(range(3), axes):
    hit_idx = np.where(hit_plane[evt] == plane)

    ht_time = hit_peaktime[evt][hit_idx]
    ht_wire = hit_wire[evt][hit_idx]
    ht_end = hit_start[evt][hit_idx]
    ht_srt = hit_end[evt][hit_idx]


    err_n = ht_srt - ht_time
    err_p = ht_time + ht_end


    ax.errorbar(ht_wire, ht_time,
                yerr=err_n, marker=None, fillstyle=None, fmt='o', alpha = 0.5 )
    #ax.scatter(ht_wire, ht_time, s=0.5)

    ax.set_xlim(xlims[plane])
    ax.set_ylim([3200, 8000])
    ax.grid(True, which="both", ls="-")

    ax.set_ylabel('Time Tick')
    ax.text(0.06,1.04, '{}-plane'.format(planes[plane]), ha='center', va='center', transform=ax.transAxes )

ax.set_xlabel('Wire Number')
plt.tight_layout()

if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/allhits.pdf')

