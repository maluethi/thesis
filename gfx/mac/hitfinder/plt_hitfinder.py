import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import argparse



parser = argparse.ArgumentParser(description='Plotting Hit Finder Example')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
args = parser.parse_args()
plt.style.use('../mythesis.mplstyle')

qula = 'low'
raw = np.load('data/raw_{}.npy'.format(qula))

qual = ['low', 'mid', 'hig']
ranges = {'low': 5290,
          'mid': 5800,
          'hig': 5620}

ylims = [[-70, 20], (-50, 50), [-10,40]]
planes = ['u', 'v', 'y']


peak_expected = ranges[qula]
peak_region = 150
ticks = np.arange(0, len(raw[0][4]))
thr_p = 6
thr_n = 6
pks = [5354, 5287, 5193]

f, axes = plt.subplots(3, 1, sharex=True)
f.set_size_inches(5.555, 8)

for raw_frame, ax, ylim in zip(raw, axes, ylims):
    #start_ticks, end_ticks, peak_ticks, peak, _ = hit_frame
    event, channel, wire, plane, adcs = raw_frame

    baseline = np.mean(adcs)
    adcs = np.array(adcs) - baseline

    rng_l = peak_expected - peak_region
    rng_H = peak_expected + peak_region

    sel_ticks = ticks[rng_l:rng_H]
    sel_adcs = adcs[rng_l:rng_H]

    ax.axhline(thr_p,0, 8000, linestyle='--', color=cm.tab20(3))

    above = np.where(sel_adcs > thr_p)[0]
    below =  np.where(sel_adcs < -thr_n)[0]

    hit_st = above[0]

    if not (plane == 2):
        hit_ed = below[-1]
    else:
        hit_ed = above[-1]

    ax.axvline(sel_ticks[hit_st], -100,100, linestyle='--', color=cm.tab20(8))
    ax.axvline(sel_ticks[hit_ed], -100,100, linestyle='--', color=cm.tab20(8))


    if not (plane == 2):
        ax.axhline(-thr_n, 0, 8000, linestyle='--', color=cm.tab20(5))


    ax.step(ticks[rng_l:rng_H], adcs[rng_l:rng_H], label='raw')
    ax.grid(True, which="both", ls="-")
    ax.set_xlim([rng_l,rng_H])
    ax.set_ylim(ylim)

    ax.text(0.15,1.04,'{}-plane, wire: {}'.format(planes[plane],wire), ha='center', va='center', transform=ax.transAxes )
    ax.set_ylabel('ADC tick')
ax.set_xlabel('Time Tick')
plt.tight_layout()

if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/hitfinder-ex-{}.pdf'.format(qula))
