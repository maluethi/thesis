import matplotlib.pyplot as plt
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='time correlation')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
args = parser.parse_args()

def make_norm_dist(x, mean, sd):
    return 1.0/(sd*np.sqrt(2*np.pi))*np.exp(-(x - mean)**2/(2*sd**2))


plt.style.use('../mythesis.mplstyle')

data = np.load('/home/data/workspace/larana/projects/out/time-histo/time-corr.npz')
df = np.load('/home/data/workspace/larana/projects/out/time-histo/time-mean.npz')

wid = df['wid']
cen = df['cen']

good = [3, 4, 7, 8]
offsets = [15, -18, 0 ,0]

maximas = data['max']
dists = data['dist']
wires = data['wire']


fig, ax = plt.subplots(1, 1)
w, h = fig.get_size_inches()
fig.set_size_inches(w, 4)

d_ax = ax.twiny()

for wire, off in zip(good, offsets):
    dist = dists[wire]
    maxim = maximas[wire]

    ax.scatter(np.array(dist) - off, maxim, label='wire={}'.format(wires[wire]), s=10)

lim = 150

ax.set_xlim([-lim, lim])
ax.set_xlabel(r'$\Delta$ wire')
ax.set_ylabel(r'correlation [A.U]')
ax.grid(True, which="both", ls="-")

d_ticks = np.linspace(-lim*0.3,lim*0.3,7)
d_ax.set_xticks(d_ticks)
d_ax.set_xlabel('$\Delta d$ [cm]')
ax.legend()

fig.tight_layout()

if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/time-corr.pdf')
