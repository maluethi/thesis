import matplotlib.pyplot as plt
import argparse
import numpy as np
from numpy import sqrt, pi, exp
import nfft

parser = argparse.ArgumentParser(description='Plotting Time Histograms')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

#matplotlib.rc('font', **font)
def gaussian(x, amp, cen, wid):
    "1-d gaussian: gaussian(x, amp, cen, wid)"
    return (amp/(sqrt(2*pi)*wid)) * exp(-(x-cen)**2 /(2*wid**2))

res = np.load('./data/histo-more-maxima-cleaned.npz')
df = np.load('./data/time-mean.npz')

wid = df['wid']
cen = df['cen']

maximas = res['max']
#error = np.concatenate(res[:,1], axis=1)
print(maximas.shape)
maximas = maximas.T

# Get the timeing
freq = 0.25
n_max = maximas.shape[1]
t = np.arange(0, n_max/freq, 1/freq)
tm = np.mean(t)
t = (t - tm)/t[-1]
stepsize = 1
wires = list(range(0, 3456, stepsize))

NN=len(maximas[0])
pow = np.zeros((750,))
fig, ax = plt.subplots(2,1)
b=0

total = np.zeros(NN)

for i in wires:
    try:

        tst = maximas[i]
        msk = np.isinf(tst)
        tstm = tst[~msk]
        tstm = (tstm - np.mean(tstm)) / (np.max(tstm) - np.min(tstm))

        N = len(t[~msk])
        ff = nfft.nfft_adjoint(t[~msk], tst[~msk], N)
        print(N)
    except:
        continue
    #plt.plot(ff.real[N/2:])
    pow = np.abs(ff)

    np.linspace(0, N)

    #ax[1].plot(pow[N/2:] + b*10, '-', alpha=0.4)

    total = total + np.pad(pow[N/2:], (0, NN-len(pow[N/2:])), 'constant')


    #ax[0].plot(tst + b)
    ax[1].plot(pow[N/2:], 'x')
    b += 10
#ax[1].plot(total, 'x')
plt.show()