import matplotlib.pyplot as plt
import argparse
import numpy as np
from numpy import sqrt, pi, exp

from larana import lar_utils as laru
import matplotlib as mpl
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable

from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

parser = argparse.ArgumentParser(description='Plotting Time Histograms')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-z', dest='zoom', action='store_true', default=False, help='production')

args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

v = 1E-6* laru.drift_speed(0.273)

print(v)
def gaussian(x, amp, cen, wid):
    "1-d gaussian: gaussian(x, amp, cen, wid)"
    return (amp/(sqrt(2*pi)*wid)) * exp(-(x-cen)**2 /(2*wid**2))

res = np.load('./data/histo-more-maxima-cleaned.npz')
df = np.load('./data/time-mean.npz')


wid = df['wid']
cen = df['cen']

maximas = res['max']
#error = np.concatenate(res[:,1], axis=1)
print(maximas.shape)
maximas = maximas.T

# Get the timeing
freq = 0.25
n_max = maximas.shape[1]
t = np.arange(0, n_max/freq, 1/freq)

window_offset = 800

if args.zoom:
    stepsize = 1

    wstart = 500
    wend = 1500

    stride = 1
    tstart = 500
    tend = 1000

    elev = 28
    azim = -30

else:
    stepsize = 10
    wstart = 0
    wend = 3455

    stride = 1
    tstart = 0
    tend = n_max

    elev = 23
    azim = -60

wires = np.arange(wstart, wend, stepsize)

fig = plt.figure()
w, h = fig.get_size_inches()
fig.set_size_inches(w, 6)

ax = fig.add_subplot(111, projection='3d')
fig.subplots_adjust(bottom=0.15,top=1.05, left=0., right=0.98)

cmap = cm.get_cmap('viridis')
norm = mpl.colors.Normalize(vmin=-15, vmax=15)
cm = cm.ScalarMappable(norm=norm, cmap=cmap)


for wire in wires:
    time = t[tstart:tend:stride]
    maxs = maximas[wire][tstart:tend:stride]

    ok = np.where(np.abs(maxs) < 20)

    add = 15 / cen[wire]


    print(cen[wire], wid[wire], add)


    c = ax.scatter([wire * 0.3]*len(time), time, maxs,  c=cm.to_rgba(maxs), alpha=0.6, s=1)

#ax.invert_zaxis()
ax.set_zlim([-20,20])
ax.set_ylabel("t [s]")
ax.set_xlabel("z [cm]")
ax.set_zlabel(r"$\Delta$t [us]")
ax.view_init(elev=elev, azim=azim)

cm.set_array(maxs*v)

axis_width = 0.7

#cb_zy = mpl.colorbar.ColorbarBase(ax_cb_zy, cmap=cmap)
cbaxes = fig.add_axes([0.1, 0.15, axis_width, 0.01])
ax_cbar = fig.colorbar(cm, cax =cbaxes, orientation="horizontal")
cbaxes.text(1.03, 0.0, r'$\Delta t$ [$\mu$s]', horizontalalignment='left',verticalalignment='center', transform=cbaxes.transAxes)


ax_dist = fig.add_axes((0.1,0.1,axis_width,0.0))
ax_dist.yaxis.set_visible(False) # hide the yaxis
ax_dist.set_xlim([-15*v,15*v])
ax_dist.text(1.03, 0.0, r'$\Delta x$ [cm]', horizontalalignment='left',verticalalignment='center', transform=ax_dist.transAxes)

ax_e = fig.add_axes((0.1,0.05,axis_width,0.0))
ax_e.yaxis.set_visible(False) # hide the yaxis
ax_e.set_xlim([-2.4,2.4])
ax_e.text(1.03, 0.0, r'$\Delta E$ [\%]', horizontalalignment='left',verticalalignment='center', transform=ax_e.transAxes)

fig.get_tight_layout()
if not args.prod:
    plt.show()
else:
    zoom = ''
    if args.zoom:
        zoom = '-zoom'
    plt.savefig('../../AP_time/timeevo_2D{}.pdf'.format(zoom))
    plt.savefig('../../AP_time/timeevo_2D-{}.png'.format(zoom), dpi=1000)
