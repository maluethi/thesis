from larana.lar_utils import read_tracks, read_laser, disassemble_laser, disassemble_track, make_figure, plot_track, \
    plot_edges
import matplotlib.pyplot as plt
import argparse


parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')

args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

fig, axes = make_figure(tex=True)
w, h = fig.get_size_inches()
fig.set_size_inches(w, 3)


entries = ([103.5, 12.2,1036.5],) # , [101.5, 12.2,1036.5], [101.5, 5,1036.5])
exits = ([91.9, 40, 0],) #, [100.2, 80, 0], [100.2, -40 , 0])


for en, ex in zip(entries, exits):
    plot_edges(axes, en, ex)


xticks = [0, 128, 256]
yticks = [-116,0,116]
zticks = [0,259, 518, 777, 1036]

axes[0].set_yticks(xticks)
#axes[0].set_xticks(zticks)

axes[1].set_yticks(yticks)

axes[2].set_xticks(xticks)
axes[2].set_yticks(yticks)


for ax in axes:
    ax.grid(True, which="both", ls="-")

if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/time-loc.pdf')