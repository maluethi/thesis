import matplotlib.pyplot as plt
import argparse
import numpy as np
from numpy import sqrt
import nfft
import scipy.fftpack as fftp


def myf(x):
    k0 = 10
    y = np.sin(50.0 * 2.0*np.pi*x) + 0.1*np.cos(80.0 * 2.0*np.pi*x)
    return y


N = 200
rng = 1000

t = np.linspace(0,rng,N)
a = myf(t)

tt = np.sort(np.random.random_sample(N)*rng)
aa = myf(tt)

fft = nfft.nfft_adjoint(t, a, N)
nff = nfft.nfft_adjoint(tt, aa, N)

f, (axu, axd) = plt.subplots(2,1)

#axu.plot(tperf, myf(tperf))
axu.scatter(t, a)
axu.scatter(tt, a)


axd.plot(np.abs(fft)[N/2:])
axd.plot(np.abs(nff)[N/2:])
plt.show()

