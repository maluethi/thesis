import matplotlib.pyplot as plt
import argparse
import numpy as np


parser = argparse.ArgumentParser(description='Plotting Time Histograms')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
args = parser.parse_args()
# Beteh-Bloch Calculations following Grupen Sec 1.1 page 4

clight = 2.9E6
K = 0.307
rho = 1.39
me = 0.511

def dEdx(p, m, z=1):
    ''' dEdx for argon:
        p: momentum of particle
        m: mass of particle
    '''

    gamma = np.sqrt(1 + np.power(p / m,2 ))
    beta = p / (gamma * m)



    alpha = 1/137
    hbar = 6.626E-34
    Na = 6.02E23



    Z = 18  # Atomic number of absorber
    A = 39.962 # Atomic weigh of absorber


    delta = 0 # np.log(28.8 * np.sqrt(1 * 1))
    I = 188.0E-6

    Ekinmax = 2 * me * np.power(beta * gamma, 2) / I

    dedx = rho * K * np.power(z,2) * Z / A * np.power(1 / beta,2) * (np.log(Ekinmax) - np.power(beta,2))

    # Do a cutoff
    idx_max = np.max(np.where(dedx > 100))
    if idx_max:
        dedx[0:idx_max] = 110

    return dedx

def dEdX_electron(p):
    gamma = np.sqrt(1 + np.power(p / me,2 ))
    beta = p / (gamma * me)

    Z = 18  # Atomic number of absorber
    A = 39.962 # Atomic weigh of absorber
    I = 188.0E-6

    Ekinmax = gamma * me / (2 * I)

    dedx = rho * K * Z / A * np.power(1 / beta, 2) * (np.log(Ekinmax) + (1-beta**2))

    return dedx

def brems_electron(p):
    gamma = np.sqrt(1 + np.power(p / me,2 ))
    beta = p / (gamma * me)
    E = gamma * me

    X0 = 14
    return E / X0


p = np.linspace(0.1,1E5, 1000000)

dedx_elec = dEdX_electron(p)
dedx_brems = brems_electron(p)

dedx_muon = dEdx(p, 105)
dedx_pion = dEdx(p, 139)
dedx_prot = dEdx(p, 938)
dedx_deut = dEdx(p, 1875)
dedx_alph = dEdx(p, 3700, z=2)



plt.style.use('../mythesis.mplstyle')

fig, ax = plt.subplots(1, 1)

ax.loglog(p, dedx_elec, linestyle='dotted', color='#1f77b4', linewidth=1.2)
ax.loglog(p, dedx_brems, linestyle='dotted', color='#1f77b4', linewidth=1.2)
ax.loglog(p, dedx_brems + dedx_elec, label=r'$e^-$')
ax.loglog(p, dedx_muon, label=r'$\mu^-$')
ax.loglog(p, dedx_pion, label=r'$\pi^-$')
ax.loglog(p, dedx_prot, label=r'$p$')
ax.loglog(p, dedx_deut, label=r'$d$')
ax.loglog(p, dedx_alph, label=r'$\alpha$')

ax.text(23,1.7,'Brems', rotation=73, color='#1f77b4')
ax.text(1.2,1.6,'Ionization', rotation=14, color='#1f77b4')


ax.set_xlim([0.1,1E5])
ax.set_ylim([1,100])

ax.set_xlabel(r'Particle Momentum [MeV/c]')
ax.set_ylabel(r'- $\frac{dE}{dx}$ [MeV/cm]')

ax.grid(True,which="both",ls="-")
ax.legend()

plt.tight_layout()

if not args.prod:
    plt.show()
else:
    plt.savefig('../../LArTPC/bethe-bloch.pdf')

