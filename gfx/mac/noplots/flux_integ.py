from scipy import integrate
import numpy as np
import matplotlib.pyplot as plt

def fl(E,th):

    cth = np.cos(th)
    p1 = 0.1
    p2 = -0.06
    p3 = 0.95
    p4 = 0.04
    p5 = 0.81

    costh = np.sqrt(
                (np.power(cth,2) + np.power(p1,2) + p2 * np.power(cth, p3) + p4 * np.power(cth, p5)) /
                (1 + p1 * p1 + p2 + p4)
                    )

    mul =  0.14 * (np.power(E * (1 + 3.64 / (E *  np.power(costh,1.29))) , -2.7))
    mu_con = 1 / (1 + 1.1 * E * costh / 115 )
    K_con = 0.054 / (1 + 1.1 * E * costh / 850 )
    return mul * (mu_con + K_con)


def fl_I(E,th):
    return fl(E, th) * np.sin(th) * 2 * np.pi






resH = integrate.nquad(fl_I, [[0.2, 1000], [0, np.pi/2]])
#resV = integrate.nquad(fl_V, [[0.2, 1000], [0, np.pi/2], [0, np.pi]])
print(f"flux: {resH[0]} cm^-2")
#print(f"flux: {resV[0]} cm^-2")


E = np.arange(0.2,10,0.1)


for thetha in [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]:
    flux = fl(E, np.deg2rad(thetha))
    plt.loglog(E, flux, label=thetha)

plt.legend()
plt.show()

