import matplotlib.pyplot as plt
import argparse
import numpy as np
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from matplotlib import cm


parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-i', dest='id', type=int, default=2, help='laser system identifier')

args = parser.parse_args()

side = args.id

plt.style.use('../mythesis.mplstyle')

data = np.load('LCS{}_azimu_scan.npy'.format(side))

ang = data[0]
counts = np.floor(data[1])

edges = data[2]
est = data[3]

green = cm.Vega20(4)
orange = cm.Vega20(2)

fig, [ax_edges, ax] = plt.subplots(2, 1)
s = fig.get_size_inches()
fig.set_size_inches(s[0], 8)


ax.plot(ang, counts, '+')

for edg in edges:
    ax.axvline(edg, 0, 50, color=orange, alpha=0.5)

rect = Rectangle((est[0], 0), edges[1] - edges[0], 50)
pc = PatchCollection([rect], facecolor=green, alpha=0.5)
ax.add_collection(pc)

#ax.grid(True,which="both",ls="-")

ax.set_xlabel(r"Polar Angle $\phi [^\circ]$")
ax.set_ylabel(r'Number of Hits')
ax.set_xlim([-70,70])
ax.set_ylim([0,25])


hist_file = 'LCS{}_histo_1000.npy'.format(side)

bins, entries = np.load(hist_file)

data = np.load('LCS{}_horiz_calib_xz.npy'.format(side))

op_angles, opening = data[1], data[2]
edges = data[3]


for angle, op in zip(op_angles, opening):
    rect = Rectangle([angle[0], 0], op, 50, fill=True, facecolor='green', alpha=0.5)
    ax_edges.add_patch(rect)
ax_edges.set_xlim([-50, 5])
ax_edges.set_ylim([0,25])
ax_edges.set_xlabel(r"Horizontal Angle $\theta [^\circ]$")
ax_edges.set_ylabel(r"Number of Hits")

ax_edges.plot(bins[:-1], entries, '+')

for lin, op_ang in zip(edges, op_angles):
    ax_edges.axvline(x=lin[0], color='red', alpha=0.3)
    ax_edges.axvline(x=lin[1], color='red', alpha=0.3)
    #ax_edges.axvline(x=op_ang[0], color='green', alpha=0.3)
    #ax_edges.axvline(x=op_ang[1], color='green', alpha=0.3)


ax_edges.text(-48,23, r'(a)')
ax.text(-65,23, r'(b)')

fig.tight_layout()


if not args.prod:
    plt.show()
else:
    plt.savefig('../../LCS-System/mirror_calib/mirror_calib-LCS{}.pdf'.format(side))
