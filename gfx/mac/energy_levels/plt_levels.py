import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from matplotlib import cm
import argparse
import numpy as np
import re
import matplotlib.gridspec as gr

parser = argparse.ArgumentParser(description='Plotting Argon Energy Levels')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
args = parser.parse_args()

def read_input():
    with open('levels.txt') as input:

        lines = [re.sub("\s+", ",", line.strip()).split(',') for line in input.readlines()]

    return lines

plt.style.use('../mythesis.mplstyle')

fig = plt.figure(figsize=(5.555, 6 ))
gs = gr.GridSpec(1,2)
gs.update(left=0.1, right=0.99, wspace=0.3)

ax_gas = plt.subplot(gs[0])
ax_liq = plt.subplot(gs[1])


x_gas_lim = [1, 20]
x_liq_lim = [30, 50]

ax_gas.hlines(0, *x_gas_lim )
ax_liq.hlines(0, *x_liq_lim, alpha=0 )

colorm = cm.get_cmap('Vega20')

last = len(read_input()) - 2
for (idx, line) in enumerate(read_input()[1:]):
    J = line[-3]
    lc = colorm(0)
    st = line[0].strip()
    Eline = float(line[-2])*1.239E-4

    alpha = 1

    if int(J) % 2 == 0:
        lc = colorm(2)
    #ax_gas.text(20, Eline, st[-2:] + f' J={J}')
    if last == idx:
        lc = colorm(4)
        alpha = 1

    ax_gas.hlines(Eline, *x_gas_lim, linewidth=1, color=lc)

    rect = Rectangle((1, Eline - 1.9), 50, 0.1)
    pc = PatchCollection([rect], edgecolors=lc, alpha=alpha)
    ax_liq.add_collection(pc)

# Ground state
rect = Rectangle((1, 0), 50, 0.1)
pc = PatchCollection([rect], edgecolors=colorm(0), alpha=alpha)
ax_liq.add_collection(pc)


Eph = 4.66
ecol = colorm(8)
for i in range(3):
    ax_liq.arrow(34, i*Eph, 0, Eph, head_width=1, head_length=0.5, color=ecol)

#ax_gas.set_xlim([0,25])
ax_gas.set_ylabel(r'Energy [eV]')

ax_liq.text(25, -0.9, r'$1s^22s^22p^63s^23p^6$', horizontalalignment='center', verticalalignment='center')

ax_gas.text(10, 1, r'gaseous', horizontalalignment='center')
ax_liq.text(45, 1, r'liquid', horizontalalignment='center')

# Hide the right and top spines
for ax in [ax_gas, ax_liq]:
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.axes.get_xaxis().set_visible(False)

    ax.set_ylim([0,18])

ax_gas.text(10,15.9, r'Ionization State',  horizontalalignment='center')
ax_liq.text(42,14, r'Ionization State',  horizontalalignment='center')
ax_liq.text(41,7, '$E_{\gamma}$ = 4.7eV',  horizontalalignment='center', color=ecol)

if not args.prod:
    plt.show()
else:
    plt.savefig('../../LCS-System/levels.pdf')

