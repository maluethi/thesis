import matplotlib.pyplot as plt
import argparse
import numpy as np


parser = argparse.ArgumentParser(description='Plotting Time Histograms')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
args = parser.parse_args()

plt.style.use('../mythesis.mplstyle')

def thetho(p, x):

    X0 = 14 #cm

    #p = 4500 #np.linspace(0.1,1E5, 1000000) # in MeV
    #x = np.linspace(1, 1000, 1000)  # in cm

    m = 105
    gamma = np.sqrt(1 + np.power(p / m, 2))
    beta = p / (gamma * m)

    tetha0 = 13.6 / (beta * p) * np.sqrt(x/X0) * (1 + 0.038 * np.log(x/(X0 * beta**2)))
    return tetha0


x_range = np.linspace(0,500,1000)
p_range = np.linspace(200, 2000, 1000)

x, p = np.meshgrid(x_range, p_range)

print(x[0])
th = thetho(p, x)

p100 = thetho(1000, x[0])


fig, ax = plt.subplots(1, 1)


levels = [0.5,1, 1.5,2,2.5,3,4,5,6,7,8,10,15,20]

cs = ax.contour(x,p,np.rad2deg(th), levels)


ax.set_xlabel(r'x [cm]')
ax.set_ylabel(r'$p_{\mu}$ [MeV]')
ax.grid(True,which="both",ls="-")
plt.clabel(cs, inline=1, fontsize=10, fmt=r'%.1f$^\circ$', manual=True)
plt.tight_layout()

if not args.prod:
    plt.show()
else:
    plt.savefig('../../LArTPC/')