import matplotlib.pyplot as plt
import argparse
import numpy as np
from matplotlib import cm
from matplotlib import colors, colorbar
from mpl_toolkits.axes_grid1 import make_axes_locatable

from larana.lar_utils import read_tracks, read_laser, disassemble_laser, disassemble_track, make_figure, plot_track, plot_edges, find_unique_polar


parser = argparse.ArgumentParser(description='Plotting Mirror Calibration')
parser.add_argument('-p', dest='prod', action='store_true', default=False, help='production')
parser.add_argument('-s', dest='id', type=int, default=1, help='laser system identifier')

args = parser.parse_args()
plt.style.use('../mythesis.mplstyle')
blue1 = cm.Vega20(0)

if args.id == 1:
    filename = "/home/data/uboone/laser/sim/Tracks-lcs1-024_true.root"
elif args.id == 2:
    filename = "/home/data/uboone/laser/sim/Tracks-lcs2-023_true.root"
else:
    raise IndexError('We have only two systems, stupid!')

tracks = read_tracks(filename, identifier="True")
lasers = read_laser(filename)

# generate event id lists, since there are more tracks than events
track_event_id = np.array([track[0] for track in tracks])
rng = -1
downsampe = 25

angles = [laser[8] for laser in lasers]
angle_steps = find_unique_polar(angles)
norm = colors.Normalize(vmin=np.min(angles), vmax=np.max(angles))
cmap = cm.get_cmap('viridis')

fig, ax = make_figure(tex=True)
w, h = fig.get_size_inches()
fig.set_size_inches(w, 7)
# loop over all tracks in the file
for idx, laser in enumerate(lasers[:rng]):

    lasr_entry, lasr_exit, dir, _, evt = disassemble_laser(laser)
    track_list = np.where(track_event_id == evt)

    # loop over all tracks in this event
    for track in tracks[track_list]:
        track_points, evt = disassemble_track(track)
        plot_track(track_points.x[::downsampe], track_points.y[::downsampe], track_points.z[::downsampe], ax,
                   linestyle="-",
                   marker="o",
                   markersize=0.5,
                   color=cm.viridis(norm(laser[8])))

divider = make_axes_locatable(ax[2])
cbar_ax = divider.append_axes("right", size="5%", pad=0.1)
cb = colorbar.ColorbarBase(cbar_ax, cmap=cmap, norm=norm, orientation='vertical')
cb.set_label('polar angle [rad]')

if not args.prod:
    plt.show()
else:
    plt.savefig('../../EField/toy_coverage{}.pdf'.format(args.id))
    plt.savefig('../../EField/toy_coverage{}.png'.format(args.id), dpi=1000)